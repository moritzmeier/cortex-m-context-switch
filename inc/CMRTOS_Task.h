#ifndef __CMRTOS_TASK_H
#define __CMRTOS_TASK_H

#include <stdint.h>
#include <stdlib.h>

#include <cross_studio_io.h>

#include "CMRTOS_Callback.h"

#define CMRTOS_MIN_STACK_SIZE 16

typedef enum
{
  NEW,
  RUNNING,
  WAITING,
} CMRTOS_TaskState_t;

typedef struct CMRTOS_Task_t
{
  unsigned char *Name;

  uint32_t *Stack;
  uint32_t StackSizeWords;
  uint32_t StackPointer;

  uint8_t Priority;
  CMRTOS_TaskState_t State;

  void (*Function)(void);

  struct CMRTOS_Task_t *Next;
} CMRTOS_Task_t;

CMRTOS_Task_t *CMRTOS_GetCurrentTask(void);
CMRTOS_Task_t *CMRTOS_CreateTask(unsigned char *name, uint32_t stackSizeWords, uint8_t priority, void (*function)(void));
void CMRTOS_DebugTasks(void);
CMRTOS_Task_t *CMRTOS_GetNextTask(void);

#endif