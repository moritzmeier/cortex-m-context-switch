#ifndef __CMRTOS_H
#define __CMRTOS_H

#include "stm32f7xx.h"

#include "CMRTOS_Time.h"
#include "CMRTOS_Task.h"
#include "CMRTOS_Callback.h"
#include "CMRTOS_Scheduler.h"

void CMRTOS_Init(void);

#endif