#ifndef __CMRTOS_TIME_H
#define __CMRTOS_TIME_H

#include <stdint.h>
#include <stdbool.h>

#include "CMRTOS_Scheduler.h"

void CMRTOS_IncrementCurrentTime(void);
uint64_t CMRTOS_GetCurrentTime(void);
uint64_t CMRTOS_GetTimeBase(void);
uint64_t CMRTOS_GetTaskInterval(void);
bool CMRTOS_SetTimeBase(uint64_t timeBase);
bool CMRTOS_SetTaskInterval(uint64_t taskInterval);

#endif