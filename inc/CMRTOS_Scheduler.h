#ifndef __CMRTOS_SCHEDULER_H
#define __CMRTOS_SCHEDULER_H

#include <stdbool.h>

#include "stm32f7xx.h"

#include "CMRTOS_Task.h"

void CMRTOS_Reschedule(void);


#endif