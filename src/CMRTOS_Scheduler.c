#include "CMRTOS_Scheduler.h"

extern CMRTOS_Task_t *CurrentTask;
CMRTOS_Task_t *NextTask;

void CMRTOS_Reschedule(void)
{
  NextTask = CMRTOS_GetNextTask();

  if(NextTask != NULL)
  {
    SCB->ICSR |= SCB_ICSR_PENDSVSET_Msk;
  }
}

__attribute__((isr, naked)) void PendSV_Handler(void)
{
  if((CurrentTask != NULL) && (CurrentTask->State == RUNNING))
  {
    asm("mrs r0, msp");

    asm("mrs r1, psp");
    asm("msr msp, r1");
    asm("isb");

    asm("push {r11}");
    asm("push {r10}");
    asm("push {r9}");
    asm("push {r8}");
    asm("push {r7}");
    asm("push {r6}");
    asm("push {r5}");
    asm("push {r4}");

    asm("mrs %0, msp" : "=r" (CurrentTask->StackPointer));

    asm("msr msp, r0");
    asm("isb");

    CurrentTask->State = WAITING;
  }

  CurrentTask = NextTask;

  if(CurrentTask->State == WAITING)
  {
    asm("mrs r0, msp");
    asm("msr msp, %0" : : "r" (CurrentTask->StackPointer));
    asm("isb");

    asm("pop {r4}");
    asm("pop {r5}");
    asm("pop {r6}");
    asm("pop {r7}");
    asm("pop {r8}");
    asm("pop {r9}");
    asm("pop {r10}");
    asm("pop {r11}");

    asm("mrs %0, msp" : "=r" (CurrentTask->StackPointer));

    asm("msr msp, r0");
    asm("isb");
  }

  CurrentTask->State = RUNNING;

  asm("msr psp, %0" : : "r" (CurrentTask->StackPointer));
  asm("isb");
}