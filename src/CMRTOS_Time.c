#include "CMRTOS_Time.h"

uint64_t CurrentTime = 0;
uint64_t TimeBase = 1;
uint64_t TaskInterval = 10;

void CMRTOS_IncrementCurrentTime(void)
{
  CurrentTime += TimeBase;

  if(CurrentTime % TaskInterval == 0)
  {
    CMRTOS_Reschedule();
  }
}


uint64_t CMRTOS_GetCurrentTime(void)
{
  return CurrentTime;
}

uint64_t CMRTOS_GetTimeBase(void)
{
  return TimeBase;
}

uint64_t CMRTOS_GetTaskInterval(void)
{
  return TaskInterval;
}


bool CMRTOS_SetTimeBase(uint64_t timeBase)
{
  if(TaskInterval % timeBase != 0)
  {
    return false;
  }

  TimeBase = timeBase;
  return true;
}

bool CMRTOS_SetTaskInterval(uint64_t taskInterval)
{
  if(taskInterval % TimeBase != 0)
  {
    return false;
  }

  TaskInterval = taskInterval;
  return true;
}