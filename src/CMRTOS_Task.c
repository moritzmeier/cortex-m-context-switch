#include "CMRTOS_Task.h"

#include <string.h>

CMRTOS_Task_t *TaskChain = NULL;
CMRTOS_Task_t *CurrentTask = NULL;

CMRTOS_Task_t *CMRTOS_GetCurrentTask(void)
{
  return CurrentTask;
}

CMRTOS_Task_t *CMRTOS_CreateTask(unsigned char *name, uint32_t stackSizeWords, uint8_t priority, void (*function)(void))
{
  CMRTOS_Task_t *newTask = (CMRTOS_Task_t *)malloc(sizeof(CMRTOS_Task_t));
  if(newTask == NULL)
  {
    CMRTOS_OutOfHeapCallback();
  }

  if(stackSizeWords < CMRTOS_MIN_STACK_SIZE)
  {
    stackSizeWords = CMRTOS_MIN_STACK_SIZE;
  }

  newTask->Function = function;
  newTask->Name = name;
  newTask->Next = NULL;
  newTask->Priority = priority;
  newTask->Stack = (uint32_t *)malloc(stackSizeWords * 4);
  if(newTask->Stack == NULL)
  {
    CMRTOS_OutOfHeapCallback();
  }

  newTask->StackPointer = (((uint32_t)newTask->Stack) + ((stackSizeWords - 8) * 4));
  newTask->StackSizeWords = stackSizeWords;
  newTask->State = NEW;

  *(newTask->Stack + (newTask->StackSizeWords - 1)) = 0x01000000;
  *(newTask->Stack + (newTask->StackSizeWords - 1) - 1) = (uint32_t)newTask->Function;
  *(newTask->Stack + (newTask->StackSizeWords - 1) - 2) = 0xFFFFFFFD;
  *(newTask->Stack + (newTask->StackSizeWords - 1) - 3) = 0x00;
  *(newTask->Stack + (newTask->StackSizeWords - 1) - 4) = 0x00;
  *(newTask->Stack + (newTask->StackSizeWords - 1) - 5) = 0x00;
  *(newTask->Stack + (newTask->StackSizeWords - 1) - 6) = 0x00;
  *(newTask->Stack + (newTask->StackSizeWords - 1) - 7) = 0x00;

  if(TaskChain == NULL)
  {
    TaskChain = newTask;
  }
  else
  {
    CMRTOS_Task_t *current = TaskChain;
    CMRTOS_Task_t *prev = NULL;
    while((current != NULL) && (current->Priority >= newTask->Priority))
    {
      prev = current;
      current = current->Next;
    }

    if(prev == NULL)
    {
      newTask->Next = current;
      TaskChain = newTask;
    }
    else
    {
      prev->Next = newTask;
      newTask->Next = current;
    }
  }

  return newTask;
}

void CMRTOS_DebugTasks(void)
{
  CMRTOS_Task_t *current = TaskChain;
  while(current != NULL)
  {
    debug_printf("Name: %s\tPriority: %d\tStack Size [WORDS]: %d\tState: %d\n", current->Name, current->Priority, current->StackSizeWords, current->State);
    current = current->Next;
  }
}

CMRTOS_Task_t *CMRTOS_GetNextTask(void)
{
  if(CurrentTask == NULL)
  {
    return TaskChain;
  }

  if(TaskChain->Priority > CurrentTask->Priority)
  {
    return TaskChain;
  }

  if(TaskChain->Priority == CurrentTask->Priority)
  {
    if(CurrentTask->Next->Priority == CurrentTask->Priority)
    {
      return CurrentTask->Next;
    }
    else
    {
      return TaskChain;
    }
  }
}